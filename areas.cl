#|
Depths:
Swamp 100
Caves 200
Ruins 350
Forest 500
|#




(defun swamp-info ()
  (case sub-state
    (top (format t "You look across the swamp, searching for the beast of the swamp. ~%")
	 (if (>= depth 100)
	     (encounter-boss)
	     (let ((swamp-string " 1. Progress into the swamp. ~%"))
	       (if optional-path
		   (setf swamp-string (combine-strings swamp-string "You look around you and notice that there is something underneath the water. ~% 2. Reach into the water. ~%")))
	       (setf swamp-string (combine-strings" 3. Exit the swamps.~%"))
	       (format t swamp-string))))
    (reach (format t "You reach into the water and ")
	   (cond ((< event-choice 10)
		  (format t "things slither and squirm past your hand. Nothing of consequence comes of it.~% "))
		 ((< event-choice 20)
		  (format t "you feel a bag. You pull out the bag and ")
		  (if (> (random 10) 2)
		      (progn (format t "find gold within the bag! You earned 40 g and 5 xp.~%")
			     (incf (player-gold player) 40)
			     (incf (player-xp player) 5))
		      (format t "find nothing. ~%")))
		 ((< event-choice 40)
		  (format t "a creature attempted to grab you. While retracting your hand from the water, you grabbed a bag. It contained a health packet! ~%")
		  (push (make-health-packet) (player-inventory player)))
		 ((<= event-choice 50)
		  (format t "a creature grabs you and tries to pull you in! Time to fight!~%")
		  (setf state 'battle)
		  (setf current-monster (make-dogwood))))
	   (setf event-choice nil
		 sub-state 'top)
	   (if (not (eq state 'battle))
	       (swamp-info)
	       (battle-info)))
    (depth (format t " You continue through the muck ~% ")
	   (test-battle)
	   (if (eq state 'swamp)
	       (swamp-info)))))

(defun cave-info ()
  (case sub-state
    (top (format t "The cave system echos various kinds of sounds. Growls, screeches, scratches, they all flood the caves into a cacophony of chaos. ~%")
	 (if (>= depth 225)
	     (encounter-boss)
	     (progn (format t " 1. Progress through the cave. ~%")
		    (if optional-path
			(format t "You look around you and notice that there is a side cavern. ~% 2. Look into the side cavern. ~%"))
		    (setf event-choice (random 50))
		    (if (> event-choice 45)
			(format t " There are strange looking rocks at your feet. ~% 3. Shuffle the rocks. ~%"))
		    (format t " 4. Exit the cave system.~%"))))
    (depth (format t " You make your way forward inside the cave system. ~% You notice something coming at you in the dark...~%")
	   (test-battle)
	   (if (eq state 'cave)
	       (random-event-tree (list " A bat flew over your head!~%"
					" A leech lurches past you.~%"
					" A mushroom shuffles past you.~%"
					" A rat dashes around you.~%"
					" A rock rushes past your head. Strangely enough, nothing attempts to attack you.~%")
				  (setf sub-state 'top)
				  (cave-info))))
    (side )
    ))
(defun ruins-info ()
  (case sub-state
    (top (format t "You look around at the crumbling ruins. It's obvious these weren't made by human beings nor on earth."))
    (depth )
    (house )
    (store-room )
    ))
(defun forest-info ()
  (case sub-state
    (top (format t "Birds are chirping. You can clearly smell the forest. Mushrooms grow here and there, and the trees are covered in moss.~%")
	 )
    (depth (random-event-tree (list "Leaves crunch under your feet as you walk through the forest.~%"
				    "You hear rustling in the bushes.~%" ;; if test-battle fails  "A deer prances away."
				    "The wind is rustling the leaves.~%"
				    "Small animals scurry along the path.~%"
				    "You can hear the howls of wolves deep in the forest.~%")
			      (test-battle)
			      (if (eq state 'forest)
				  (progn (if (eq event-num 1)
					     (format t " A Deer prances away into the brush. ~%~%"))
					 (setf sub-state 'top)
					 (forest-info)))))
    (tree )
    ))
(defun mountain-info ()
  (if current-monster
      (format t " The Dragon greets you.
 \"Welcome Young Warrior. You have proven yourself worthy to battle me! Come forward and fight like a human! Or run like a child!\"

If you wish to escape, type in run, otherwise type in 1. ~%")
      (format t " Upon the King of Dragons death, the ground shakes. You hear commotion from all over, birds flying away and monstrosities fleeing.
Within moments, the monstrosities start dying. Falling from the sky. Floating to the surface of lakes. Tumbling down mountains.
You have killed not just the King of Dragons, but rather, the King of DEMONS! He was the chain linking your world and his world, allowing for the demons to flourish here. Without him, they can't live in this world.


Congratulations! You have liberated your world from the clutches of demons! A new world has been birthed and the full realization of humanity is now possible!

Enter anything to exit. ~%")))

(defun town-info ()
  (format t " You are in town. ~% Horses are pulling carts, rotten fruit litters the ground, the local bar is open and the town shop is open. ~% What will you do? ~%")
  (format t " 1. Go to World Map. ~% 2. Shop. ~% 3. Bar. ~%"))

(defun bar-info ()
  (format t " You are in the town bar. ~% The smell of freshly spilt mead fills the air. You can hear typical bar chatter: boasting about unproven kills, arguing about how many arrows it would take to down a wyvern, discussions about the old world. ~% 1. Buy a drink ~% 2. Exit the bar. ~%"))

(defun shop-info ()
  (format t " You walk into one of the corner stores, advertising 'Weapons, Armor and Health!'. The smell of sweat and frustration fills your nostrils.
The shopkeed notices you. \"Ah! Come in, come in weary traveler! Welcome to the local shop! We have blades, rebar, armor, and healing items! So long as you've got the gold that is.\"
The shopkeep looks you up and down. \"Hm...if you have any items that you don't need, we will gladly buy them off you at the going rate!\"

 1. to look at what you can buy ~% 2. to sell ~% 3. to exit. ~%"))

(defun buy-drink ()
  (if (>= (player-gold player) 10)
      (progn (format t " You pay the barkeep 10 g for a pint of mead, restoring 5 hp.~%")
	     (decf (player-gold player) 10)
	     (incf (player-hp player) 5))
      (format t " The Barkeep yells at you. ~% \"Oi! What de fak do ye tenk yer dein ya sun oth a bech! Git de fook oot mei ber if yer gonna do dis shit ye cheeky coont!\" ~%")))

(defun world-info ()
  (format t "         You are at a local crossroad. 
The Town is located to the west. Several wagons of goods are coming from and going to the town. 
You can see the local swamp to the northwest, with what you believe to be an occasional gas explosion.
To the North, near the base of the dragon's lair, the mountain, is a cave system.
The metallic marble ruins lie to the southeast. You can see a large structure moving somewhere in the back of the ruins.
An overgrown forest, full of what were once mythical beasts, lies to the east.
   1. Town || 2. Swamp || 3. Cave System || 4. Ruins || 5. Forest || 6. Mountain -- Dragon's Lair ~%"))
