(defvar *default-colored-bits* 4)
(defun combine-strings (str &rest strings)
  "Used on strings that just need to be concatenated together, but not inserting a newline at the end of each of them."
  (loop for s in strings
     do (if (not (stringp s))
	    (setf s (write-to-string s)))
       (setf str (concatenate 'string str s)))
  str)

(defvar 4bit-colors '((black 30)
		      (red 31)
		      (green 32)
		      (yellow 33)
		      (blue 34)
		      (magenta 35)
		      (cyan 36)
		      (white 37)
		      (bright-black 90)
		      (dark-grey 90)
		      (bright-red 91)
		      (bright-green 92)
		      (bright-yellow 93)
		      (bright-blue 94)
		      (bright-magenta 95)
		      (bright-cyan 96)
		      (bright-white 97)))
(defun get-fg-color (color)
  (cadr (assoc color 4bit-colors)))
(defun get-bg-color (color)
  (+ (cadr (assoc color 4bit-colors)) 10))

(defun 4bit-colored-string (str color-code &key (bg-color 0))
  (cond ((and (integerp color-code)
	      (integerp bg-color))
	 (concatenate 'string
		      (string #\ESC) "[" (write-to-string color-code) "m"
		      (string #\ESC) "[" (write-to-string bg-color) "m"
		      str (string #\ESC) "[0m"))
	((integerp color-code)
	 (concatenate 'string
		      (string #\ESC) "[" (write-to-string color-code) "m"
		      (string #\ESC) "[" (write-to-string (get-bg-color bg-color)) "m"
		      str (string #\ESC) "[0m"))
	((integerp bg-color)
	 (concatenate 'string
		      (string #\ESC) "[" (write-to-string (get-fg-color color-code)) "m"
		      (string #\ESC) "[" (write-to-string bg-color) "m"
		      str (string #\ESC) "[0m"))
	(t
	 (concatenate 'string
		      (string #\ESC) "[" (write-to-string (get-fg-color color-code)) "m"
		      (string #\ESC) "[" (write-to-string (get-bg-color bg-color)) "m"
		      str (string #\ESC) "[0m"))))

(defun 8bit-colored-string (str color-code &key bg-color)
  (if bg-color
      (concatenate 'string (string #\ESC) "[38;5;" (write-to-string color-code) "m" (string #\ESC) "[48;5;" (write-to-string bg-color) "m"str (string #\ESC) "[0m")
      (concatenate 'string (string #\ESC) "[38;5;" (write-to-string color-code) "m" str (string #\ESC) "[0m")))

(defun 24bit-colored-string (str color-code &key bg-color)
  "Some terminals on linux do not support 24 bit (r g b) colors. For Maximum support, use 8bit colored strings."
  (let* ((fg-r (write-to-string (car color-code)))
	 (fg-g (write-to-string (cadr color-code)))
	 (fg-b (write-to-string (caddr color-code)))
	 (fg-color (concatenate 'string fg-r ";" fg-g ";" fg-b)))
    (if bg-color
	(let* ((bg-r (write-to-string (car color-code)))
	       (bg-g (write-to-string (cadr color-code)))
	       (bg-b (write-to-string (caddr color-code)))
	       (bg-color (concatenate 'string bg-r ";" bg-g ";" bg-b)))
	  (concatenate 'string (string #\ESC) "[38;2;" fg-color "m" (string #\ESC) "[48;2;" bg-color "m" str (string #\ESC) "[0m"))
	(concatenate 'string (string #\ESC) "[38;2;" fg-color "m" str (string #\ESC) "[0m"))))

(defun colored-string (str text-color &key bg-color (bits *default-colored-bits*))
  (case bits
    (4 (if bg-color
	   (4bit-colored-string str text-color :bg-color bg-color)
	   (4bit-colored-string str text-color)))
    (8 (if bg-color
	   (8bit-colored-string str text-color :bg-color bg-color)
	   (8bit-colored-string str text-color)))
    ;;;;https://en.wikipedia.org/wiki/ANSI_escape_code#8-bit  <-  Link to the 256 colors available in 8bit mode
    (24 (if bg-color
	   (24bit-colored-string str text-color :bg-color bg-color)
	   (24bit-colored-string str text-color)))))
#|
This is an example of a way to do a roguelike without a curses binding

(defun interactive-terminal ()
  (let ((running t))
    (loop while running
       do (let ((key (trivial-raw-io:read-char)))
	    (if (char= key #\e)
		(setf running nil)
		(print key)))
	 (sleep .1)
	 (clear-output))))
The above would require that you make a new stream to hold the string and print to the screen
|#
