(defstruct item
  name
  (attack 0)
  (defense 0)
  (heal 0)
  (cost 0))
(defvar shop-items nil)
(defvar item-builders nil)
(defmacro def-item (item &key (name "item") (attack 0) (defense 0) (heal 0) (cost 100))
  (let ((func-name (intern (string-upcase (concatenate 'string "make-" (symbol-name item))))))
    (push `',func-name item-builders)
    `(defstruct (,item (:include item
				 (name ,name)
				 (attack ,attack)
				 (defense ,defense)
				 (heal ,heal)
				 (cost ,cost))))))

(def-item dagger :name "Dagger" :attack 20 :cost 100)
(def-item short-sword :name "Short Sword" :attack 30 :cost 1000)
(def-item longsword :name "Longsword" :attack 50 :cost 2500)
(def-item giant-sword :name "Giant Sword" :attack 80 :cost 5000)
(def-item rebar :name "Rebar" :attack 200 :cost 10000)
(def-item demon-slayer :name "Demon Slayer" :attack 2000 :cost 1000000)
(def-item cloth :name "Cloth" :defense 5 :cost 100)
(def-item leather :name "Leather" :defense 20 :cost 200)
(def-item iron :name "Iron" :defense 60 :cost 800)
(def-item steel :name "Steel" :defense 120 :cost 1600)
(def-item chain-mail :name "Chain Mail" :defense 240 :cost 4800)
(def-item dark-armor :name "Dark Armor" :defense 600 :cost 500000)
(def-item health-packet :name "Health Packet" :heal 5 :cost 100)
(def-item health-potion :name "Health Potion" :heal 20 :cost 200)
(def-item health-salve :name "Health Salve" :heal 80 :cost 1000)
(def-item health-kit :name "Health Kit" :heal 1000 :cost 10000)
(loop for item in item-builders
   do (push (funcall (eval item)) shop-items))

(setf item-builders (reverse item-builders))

(defun buy-info ()
  (format t "  0. Exit Buying ~%")
  (loop for x below (length shop-items)
     do (let* ((item (nth x shop-items))
	       (info-type (if (> (item-attack item) 0)
			      "Damage:"
			      (if (> (item-defense item) 0)
				  "Defense:"
				  "Heals for:")))
	       (amount (if (> (item-attack item) 0)
			   (item-attack item)
			   (if (> (item-defense item) 0)
			       (item-defense item)
			       (item-heal item))))
	       (cost (item-cost item)))
	  (format t " ~2D. ~15A - ~10A ~8D   Cost: ~9D ~%"
		  (1+ x) (item-name item)
		  info-type amount cost))))

(defun list-sell-inventory ()
  (format t " Inventory: ~%")
  (format t "  0. Exit Selling ~%")
  (loop for item in (player-inventory player)
     do (let ((name (item-name item))
	      (gold (item-cost item))
	      (item-no (position item (player-inventory player))))
	  (format t " ~3D. ~15A  G: ~10A ~%" (1+ item-no) name (colored-string (write-to-string gold) 226 :bits 8)))))
#|  (loop for i from 0 to (1- (length (player-inventory player)))
     do (let ((name (item-name (nth i (player-inventory player))))
	      (gold (item-cost (nth i (player-inventory player)))))
	  (format t " ~3D. ~15A  G: ~10A ~%" (1+ i) name (colored-string (write-to-string gold) 226 :bits 8)))))|#

(defun sell-info ()
  (format t " What do you wish to sell? ~%")
  (list-sell-inventory))


