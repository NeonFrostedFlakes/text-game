(load "string.cl")
(load "file.cl")
(defstruct player
  (name "")
  (max-hp 50)
  (hp 50)
  (attack 10)
  (defense 10)
  (bonus-defense 0)
  (level 1)
  (xp 0)
  (to-next-level 40)
  race
  class
  weapon
  armor
  inventory
  (gold 100)
  (area 'town))
(defvar finished nil)
(defvar depth 0)
(defvar player (make-player))
(defvar player-dead nil)
(defvar state 'town)
(defvar sub-state 'top)
(defvar old-state nil)
(defvar old-sub-state nil)
(defvar current-monster nil)
(defvar message nil)
(defvar game-title "[Game Title]")
(load "shop.cl")
(load "monsters.cl")
(load "battle.cl")
(load "alt-choices.cl")
(load "areas.cl")
(load "actions.cl")

(defun randval (n)
  (1+ (random (max 1 n))))

(defun display-intro ()
  (format t "                                        Welcome to ~A! ~% 
 The laws of causality have led to this very moment, the moment you would decide to use this product for play, the decisive moment that you choose whether to continue forward into the gaping maws of eternity or to fall into the abyss of madness! 
~% You were a simple child growing up. Somewhat selfish, but determined to help people. You've heard tales of the oldworld, the time before the great awakening when worlds collided with one another, destroying machines that dwarfed dragons. Many of this world stood no chance against these creatures, due to small projectiles being ineffective against them. The world's militaries fell to these beasts within a matter of weeks, soon infesting the world over. ~% Those left over from the mass slaughter came together to try to reform society. Some were successful in establishing new relations of production, but many fell back into the old ways, dooming themselves to failure. You were born in one of these experimental settlements, Red York of the United Councils of America, and are determined to rid the world of these monstrosities.
 About a week ago, you arrived in Neo-Yugoslavia, determined to destroy as many of the strongest creatures in the area. The townsfolk had told you of the dragon atop the tallest local mountain, the one that proclaimed itself the king of dragons. Supposedly, it had already killed thousands of well-trained soldiers and adventurers. However, they are having issues with creatures in the swamp, cave, ruins, and forest. Killing creatures in these areas will give you a better sense of the local monsters and net you some money for various old-world healing supplies.
 Shortly after the locals told you of their problems, a roar had erupted from the mountain. A challenge from the King Of Dragons!
~%~% Type in equip to equip weapons or armor, help to bring up help, inventory to display your inventory, or an integer (0, 1, 2, 3...) to make a choice. ~%" (colored-string game-title 124 :bits 8)))

(defun display-help ()
  (format t "                                        Welcome to ~A! ~% 
  ~% Type in equip to equip weapons or armor, help to bring up this information, intro to review the introduction, or an integer (0, 1, 2, 3...) to make a choice. ~%" (colored-string game-title 124 :bits 8)))

(defun init ()
  (format t "What is thy Race? ~%") ;;then test for a class and race
  (format t "1. Human  2. Elf  3. Dwarf ~%")
  (finish-output nil)
  (finish-output t)
  (let ((ip (read-line)))
    (if (> (length ip) 1)
	(setf (player-race player) (intern (string-upcase ip)))
	(case ip
	  (1 (setf (player-race player) 'human))
	  (2 (setf (player-race player) 'elf))
	  (3 (setf (player-race player) 'dwarf)))))
  (if (not (or (eq (player-race player) 'human)
	       (eq (player-race player) 'elf)
	       (eq (player-race player) 'dwarf)))
      (setf (player-race player) 'human))
  (format t "What is thy Name? ~%")
  (finish-output nil)
  (finish-output t)
  (setf (player-name player) (read-line))) ;;get player input for name

(defun display-info ()
  (let ((optional-paths '(swamp forest ruins cave))
	(op-t (random 2)))
    (if (find state optional-paths)
	(if (eq op-t 1)
	    (setf optional-path t)
	    (setf optional-path nil))))
  (case state
    (town  (town-info))
    (bar (bar-info))
    (shop (format t "~% Available gold: ~A~%" (colored-string (write-to-string (player-gold player)) 226 :bits 8))
	  (case sub-state
	    (top (shop-info))
	    (sell (sell-info))
	    (buy (buy-info))))
    (world (world-info))
    (equip (case sub-state
	     (top (equip-info))
	     (weapon (weapon-info))
	     (armor (armor-info))))
    (battle (case sub-state
	      (top (battle-info))
	      (items (list-health-items))))
    (cave (cave-info))
    (forest (forest-info))
    (ruins (ruins-info))
    (swamp (swamp-info))
    (mountain (mountain-info))
    (use (use-info))))

(defun process-choice (choice)
  (case state
    (town (case choice
	    (1 (set-area 'world))
	    (2 (setf state 'shop))
	    (3 (setf state 'bar))))
    (bar (case choice
	   (1 (buy-drink))
	   (2 (exit-bar))))
    (shop (case sub-state
	    (top (case choice
		   (1 (setf sub-state 'buy))
		   (2 (setf sub-state 'sell))
		   (3 (setf state 'town))))
	    (buy (if (eq choice 0)
		     (setf sub-state 'top)
		     (if (< (1- choice) (length item-builders))
			 (buy (funcall (eval (nth (1- choice) item-builders))))
			 (format t "Sorry hun, don't got dat item.~%"))));;shop-items))))
	    (sell (if (eq choice 0)
		      (setf sub-state 'top)
		      (sell (1- choice))))))
    (world (case choice
	     (1 (set-area 'town))
	     (2 (set-area 'swamp))
	     (3 (set-area 'cave))
	     (4 (set-area 'ruins))
	     (5 (set-area 'forest))
	     (6 (if (> (player-level player) 30)
		    (progn (set-area 'mountain)
			   (setf current-monster (make-mountain-dragon)))
		    (format t " The dragon rests atop the mountain. You are still too weak. You need to be at least level 31 to fight the dragon. ~%")))
	     (otherwise (princ "Invalid Choice."))
	     ))
    (equip (case sub-state
	     (top (case choice
		    (1 (setf sub-state 'weapon))
		    (2 (setf sub-state 'armor))
		    (3 (exit-equip))))
	     (weapon (equip-weapon choice))
	     (armor (equip-armor choice))))
    (battle (case sub-state
	      (top (case choice
		     (1 (attack))
		     (2 (defend))
		     (3 (setf sub-state 'items))
		     (4 (run)))
		   (battle))
	      (items (if (not (eq choice 0))
			 (progn (use-item (1- choice))
				(battle))
			 (setf sub-state 'top)))))
    (swamp (case choice
	     (1 (increase-depth))
	     (2 (reach-into-water))
	     (3 (exit-area 'swamp))))
    (cave (case choice
	   (1 (increase-depth))
	   (2 (explore-side-cave))
	   (3 (shuffle-rocks))
	   (4 (exit-area 'cave))))
    (ruins (case choice
	     (1 (increase-depth))
	     (2 )
	     (3 (exit-area 'ruins))))
    (forest (case choice
	     (1 (increase-depth))
	     (2 )
	     (3 (exit-area 'forest))))
    (mountain (if current-monster
		  (setf state 'battle)
		  (setf finished t)))
    (use (case choice
	   (0 (setf state old-state
		    sub-state old-sub-state
		    old-state nil
		    old-sub-state nil))
	   (otherwise (use-item (1- choice)))
	   ))))

#|
(defun save-game ()
  (mulitple-value-bind (sec min hr day month year dow dst-p tz)
		       (get-decoded-time)
		       (declare (ignore dow dst-p tz))
		       (let ((filename (combine-strings (sb-posix:getcwd) "/" (player-name player) "--" day "-" month "-" year "--" hr "h" min "m" sec "s.tgs")))
			 (with-open-file (file filename :direction :output :if-exists :supersede)
			   (print player file)))))

(defun display-saves ()
  (let ((current-folder (concatenate 'string (sb-posix:getcwd) "/"))
	(files nil))
    (setf files (get-directory-files current-folder))
    (loop for x below (length files)
       do (format t " ~D. ~A ~%" x (nth x files)))))

(defun load-game (file-number)
  (let ((current-folder (concatenate 'string (sb-posix:getcwd) "/"))
	(files nil))
    (setf files (get-directory-files current-folder))
    (if (find-non-numbers file-number)
	(format t "Error! Non-numbers present in file number!")
	(let ((file-number (parse-integer file-number)))
	  (if (< file-number (length files))
	      (with-open-file (file (combine-strings (sb-posix:getcwd) "/" (nth file-number files)) :direction :input)
		(setf player (read file))
		(setf state (read file))
		(setf sub-state (read file))
		(setf current-monster (read file))))))))
|#
;;;;Probably expand upon the saving feature to create a new folder called saves to store the files in

(defun game-loop ()
  (display-info)
  (if (or (eq state 'world)
	  (eq state 'town))
      (format t "Type help if you need more information. ~%"))
  (if message
      (progn (format t " ~A ~%" message)
	     (setf message nil)))
  (if (> (player-hp player) (player-max-hp player))
      (setf (player-hp player) (player-max-hp player)))
  (let ((color (cond ((eq (player-area player) 'swamp)
		      34)
		     ((eq (player-area player) 'forest)
		      77)
		     ((eq (player-area player) 'mountain)
		      242)
		     ((eq (player-area player) 'ruins)
		      58)
		     ((eq (player-area player) 'cave)
		      81)
		     ((eq (player-area player) 'town)
		      88)
		     ((eq (player-area player) 'bar)
		      94)
		     ((eq (player-area player) 'shop)
		      220)
		     ((eq (player-area player) 'world)
		      39)
		     (t
		      512))))
    (format t "~A:>" (colored-string (write-to-string state) color :bits 8)))  
  (finish-output nil)
  (finish-output t)
  (let ((choice (read-line)))
    (fresh-line)
    (if (> (length choice) 0)
	(if (find-if #'alpha-char-p choice)
	    (cond ((string-equal choice "Equip")
		   (if (and (not old-state)
			    (not old-sub-state))
		       (equip-state)))
		  #|(string-equal choice "Weapon")
		    (print-weapon)
		   (string-equal choice "Armor")
		  (print-armor)
		  |#
		  ((string-equal choice "Help")
		   (display-help))
		  ((or (string-equal choice "Inventory")
		       (string-equal choice "inv"))
		   (list-player-inventory))
		  ((string-equal choice "INTRO")
		   (display-intro))
		   ((string-equal choice "STATS")
		    (display-stats))
		   ((string-equal choice "EXIT")    
		    (setf finished 't))
		   ((string-equal choice "DEPTH")
		    (format t "~%~% Your current Depth: ~D ~%~%" depth))
		   ((string-equal choice "USE")
		    (if (eq state 'battle)
			(format t "~%~%You cheeky cunt...Use your turn to use an item!~%~%")
			(if (and (not old-state)
				 (not old-sub-state))
			    (setf old-state state
				  old-sub-state sub-state
				  state 'use
				  sub-state 'top))))
		   ((string-equal choice "RUN")
		    (if (eq state 'mountain)
			(progn (format t "
To think that you, the brave warrior who would face the dragon alone, by yourself, in this godforsaken wasteland,
would run from the battle of the century! Truly remarkable, isn't it?
The universe rewards struggle, not cowardice!~%");;something something, cowardice
			       (set-area 'world))))
		   #|	      ((string-equal choice "SAVE")
		   (save-game))
		   ((string-equal choice "SAVES")
		   (display-saves))
		   (t
		   (if (find #\space choice)
		   (let ((sub-choice (subseq choice 0 (position #\space choice)))
		   (int-choice (subseq choice (1+ (position #\space choice)))))
		   (if (string-equal "load" sub-choice)
		   (load-game int-choice)))
		   (format t " Invalid choice. ~%")))|#
		   )
	    (process-choice (parse-integer choice)))))
  (if (> (player-hp player) 0)
      (if (not finished)
	  (game-loop))
      (format t " A /pol/ack teleports behind you and says: \"Heh, nothing personnel kid.\" ~% You have been stabbed by a katana wielding crypto-nazi. Game over. ~%")))

(defun display-exit ()
  (format t " Thank you for playing ~A. ~% Press any key to exit. ~%" (colored-string game-title 124 :bits 8))
  (let ((choice (read-line)))
    (fresh-line))
  )

(defun txt-game ()
  (setf player (make-player :weapon (make-dagger) :armor (make-cloth) :inventory (list (make-health-potion)))
	state 'town
	finished nil)
  (display-intro)
  (init)
  (game-loop)
  (display-exit))

(defun main ()
  (txt-game))
