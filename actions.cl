;;travel

(defun exit-area (area)
  (case area
    (swamp (format t " You make your way out of the muck that they call the swamp, back to the main roads. ~%"))
    (cave (format t " You luckily are able to make your way out of the maze of caverns, back to the surface. ~%"))
    (ruins (format t " You make your way out of the crumbling ruins, back to a local road. ~%"))
    (forest (format t " You make your way out of the dense forest, full of brambles and other plants that could decimate an army, back to a local clearing.")))
  (setf state 'world
	sub-state 'top
	depth 0
	(player-area player) 'world))

(defun equip-info ()
  (format t " What do you wish to equip? ~% 1. Weapon ~% 2. Armor ~% 3. Exit ~%"))

(defun equip-state ()
  (setf old-state state
	old-sub-state sub-state
	state 'equip
	sub-state 'top))

(defun weapon-info ()
  (loop for item in (player-inventory player)
     do (if item
	    (if (> (item-attack item) 0)
		(format t " ~D ~A A: ~D~%" (position item (player-inventory player)) (item-name item) (item-attack item)))))
  (format t " ~D EXIT ~%" (length (player-inventory player)))
  (format t "Current Weapon: ~A A: ~D~%" (item-name (player-weapon player)) (item-attack (player-weapon player))))

(defun weapon-state ()
  (setf sub-state 'weapon))

(defun equip-weapon (item-number)
  (if (< item-number (length (player-inventory player)))
      (if (> (item-attack (nth item-number (player-inventory player))) 0)
	  (if (player-weapon player)
	      (progn (push (player-weapon player) (player-inventory player))
		     (setf (player-weapon player) (nth (1+ item-number) (player-inventory player))
			   (nth (1+ item-number) (player-inventory player)) nil
			   (player-inventory player) (remove nil (player-inventory player))))
	      (setf (player-weapon player) (nth item-number (player-inventory player))
		    (nth item-number (player-inventory player)) nil
		    (player-inventory player) (remove nil (player-inventory player))))
	  (setf message "Invalid option. Please select a valid non-negative integer."))
      (setf sub-state 'top)))

(defun armor-info ()
  (loop for item in (player-inventory player)
     do (if (> (item-defense item) 0)
	    (format t " ~D. ~A D: ~D~%" (position item (player-inventory player)) (item-name item) (item-defense item))))
  (format t " ~D EXIT ~%" (length (player-inventory player)))
  (format t "Current Armor: ~A D: ~D~%" (item-name (player-armor player)) (item-defense (player-armor player))))

(defun armor-state ()
  (setf sub-state 'armor))

(defun equip-armor (item-number)
  (if (< item-number (length (player-inventory player)))
      (if (> (item-defense (nth item-number (player-inventory player))) 0)
	  (if (player-armor player)
	      (progn (push (player-armor player) (player-inventory player))
		     (setf (player-armor player) (nth (1+ item-number) (player-inventory player))
			   (nth (1+ item-number) (player-inventory player)) nil
			   (player-inventory player) (remove nil (player-inventory player))))
	      (setf (player-armor player) (nth item-number (player-inventory player))
		    (nth item-number (player-inventory player)) nil
		    (player-inventory player) (remove nil (player-inventory player))))
	  (setf message "Invalid option. Please select a valid non-negative integer."))
      (setf sub-state 'top)))

(defun exit-equip ()
  (format t "Returning to ~a. ~%" old-state)
  (setf state old-state
	sub-state old-sub-state
	old-state nil
	old-sub-state nil))



(defun list-player-inventory ()
  (loop for item in (player-inventory player)
     do (let ((in (item-name item))
	      (ia (item-attack item))
	      (id (item-defense item))
	      (ih (item-heal item))
	      (ig (item-cost item)))
	  (format t " ~D. ~A A: ~D  D: ~D  H: ~D  G: ~D ~%" (position item (player-inventory player)) in ia id ih ig))))

(defun use-info ()
  (format t "~% 0 to get out of the use item menu, otherwise tries to use an item.~%")
  (loop for item in (player-inventory player)
     do (let ((in (item-name item))
	      (ia (item-attack item))
	      (id (item-defense item))
	      (ih (item-heal item))
	      (ig (item-cost item)))
	  (format t " ~D. ~A A: ~D  D: ~D  H: ~D  G: ~D ~%" (1+ (position item (player-inventory player))) in ia id ih ig))))


;;shop

(defun exit-bar ()
  (format t " Several travelers fall out off their stools as you make way towards the exit. Pissed off, they get into a brawl. ~% You hastily exit before you get caught up in their shenanigans. ~%")
  (setf state 'town))

(defun buy (item)
  (if (>= (player-gold player) (item-cost item))
      (progn (if (player-inventory player)
		 (setf (player-inventory player) (append (player-inventory player) (list item)))
		 (push item (player-inventory player)))
	     (format t "Thank ye kindly lass! Here's your ~A.~%" (item-name item))
	     (setf (player-gold player) (- (player-gold player) (item-cost item))))
      (format t "You bloody son of a bitch! You don't have enough gold for that item! ~%")))

(defun sell (item-no)
  (let ((item (nth item-no (player-inventory player))))
    (if item
	(progn (format t "Thank Ye kindly! Here's your ~D g. ~%" (item-cost item))
	       (setf (player-gold player) (+ (item-cost item) (player-gold player))
		     (nth item-no (player-inventory player)) nil
		     (player-inventory player) (remove nil (player-inventory player))))
	(format t "Sorry lass, I don't buy air. ~%"))))

(defun exit-shop ()
  (format t " The Shopkeep waves you goodbye as you walk outside, back into town. ~%")
  (setf state 'town))

;;battle

(defun attack ()
  (setf sub-state 'player-attack))
(defun defend ()
  (setf (player-bonus-defense player) (round (/ (+ (player-defense player) (item-defense (player-armor player))) 4))
	sub-state 'monster-turn))
(defun list-health-items ()
  (loop for item in (player-inventory player)
     do (if (> (item-heal item) 0)
	    (format t " ~D ~A H: ~D~%" (1+ (position item (player-inventory player))) (item-name item) (item-heal item))))
  (format t " 0. EXIT ~%"))

(defun use-item (item-num)
  (if (< item-num (length (player-inventory player)))
      (progn (if (> (item-heal (nth item-num (player-inventory player))) 0)
		 (if (< (player-hp player) (player-max-hp player))
		     (progn (incf (player-hp player) (item-heal (nth item-num (player-inventory player))))
			    (format t " You used a ~A to heal for ~D!~%"
				    (item-name (nth item-num (player-inventory player)))
				    (item-heal (nth item-num (player-inventory player))))
			    (setf (nth item-num (player-inventory player)) nil
				  (player-inventory player) (remove nil (player-inventory player))))
		     (format t " You are already at max health. There is no point in using this item.~%"))
		 (format t " Either A: That is not a healing item or B: it is an invalid choice. Please select a valid item. ~%"))
	     (if (eq state 'battle)
		 (setf sub-state 'monster-turn)))
      (format t "~% Invalid item. ~% ~%")))

(defun run ()
  (let ((run-attempt (random 100)))
    (if (not (mountain-dragon-p current-monster))
	(if (> run-attempt 25)
	    (end-battle :ran t)
	    (progn (format t " You couldn't run!~%")
		   (setf sub-state 'monster-turn)))
	(progn (format t " Foolish mortal! You wish to run from my challenge!? You shall pay for this incolence! ~% The King of Dragons slashed you, past your armor, for 500 damage! ~%")
	       (decf (player-hp player) 500)))))

(defun display-stats ()
  (format t "~A's stats: ~% Lvl: ~D ~% HP/Max HP: ~D/~D~% Atk: ~D ~% Def: ~D ~% XP/TNL: ~5D/~D ~% Gold: ~D~%"
	  (player-name player) (player-level player)
	  (player-hp player) (player-max-hp player)
	  (player-attack player) (player-defense player)
	  (player-xp player) (player-to-next-level player)
	  (player-gold player)))
