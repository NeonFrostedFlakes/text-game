(defstruct monster
  (name "Monster!")
  (hp 10)
  (max-hp 10)
  (defense 10)
  (attack 10)
  (attack-message "The monster attacks!"))
(defvar monster-builders (make-hash-table))

(defmacro defmonster (monster-struct &key (name "monster") (hp 1) (attack 1) (attack-message "slashed") area)
  (let ((func-name (intern (string-upcase (concatenate 'string "make-" (symbol-name monster-struct))))))
    (push `',func-name (gethash (eval area) monster-builders))
    `(defstruct (,monster-struct (:include monster
					   (name ,name)
					   (hp ,hp)
					   (max-hp ,hp)
					   (defense (ceiling (/ (+ ,attack ,hp) 15)))
					   (attack ,attack)
					   (attack-message ,attack-message)))))) ;;(intern (string-upcase (concatenate 'string "#'make-" ,name))) ,area))
(defmacro defboss (boss-struct &key (name "boss") (hp 10000) (attack 100) (attack-message "tore a new asshole for"))
  `(defstruct (,boss-struct (:include monster
				      (name ,name)
				      (hp ,hp)
				      (max-hp ,hp)
				      (defense (ceiling (/ (+ ,attack ,hp) 15)))
				      (attack ,attack)
				      (attack-message ,attack-message)))))
(defun build-monster (monster-num)
  (setf current-monster (funcall (eval (nth monster-num (gethash (player-area player) monster-builders))))))

(defun gen-monster ()
  (let* ((area (player-area player))
	 (monster-num (random (length (gethash area monster-builders))))
	 (valid-monster nil))
    (loop while (not valid-monster)
       do (case area
	    (swamp (if (and (< depth 75)
			    (eq monster-num 0))
		       (setf monster-num (random (length (gethash area monster-builders))))
		       (setf valid-monster t)))
	    (cave (if (and (< depth 150)
			    (eq monster-num 0))
		       (setf monster-num (random (length (gethash area monster-builders))))
		       (setf valid-monster t)))
	    (ruins (if (and (< depth 300)
			    (eq monster-num 0))
		       (setf monster-num (random (length (gethash area monster-builders))))
		       (setf valid-monster t)))
	    (forest (if (and (< depth 400)
			    (eq monster-num 0))
			(setf monster-num (random (length (gethash area monster-builders))))
			(setf valid-monster t)))))
    (build-monster monster-num)))

(defun attack-message (dmg)
  (format t " The ~A ~A you for ~A damage! ~%" (monster-name current-monster) (monster-attack-message current-monster) (colored-string (write-to-string dmg) 6 :bg-color 1 :bits 8)))

;;;;Swamp

(defmonster swamp-thing :name "Swamp Thing" :hp 20 :attack 2 :area 'swamp)
(defmonster dragon-fly :name "Giant Dragon Fly" :hp 10 :attack 1 :attack-message "dusted" :area 'swamp)
(defmonster toad :name "Toad" :hp 20 :attack 1 :attack-message "drugged" :area 'swamp)
(defmonster dogwood :name "Sentient Dogwood" :hp 1 :attack 1 :attack-message "slapped" :area 'swamp)
(defmonster wisp :name "Friar's Lantern" :hp 1 :attack 5 :attack-message "ignites" :area 'swamp)
(defmonster swamp-ogre :name "Swamp Ogre" :hp 100 :attack 40 :attack-message "yelled \"GET OUT OF MEH SWEMP!\" at" :area 'swamp)

(defboss the-deep :name "The Deep" :hp 1000 :attack 40 :attack-message "splashed") ;Boss of the Swamp

;;;;Cave

(defmonster eyeball :name "Floating Eyeball" :hp 200 :attack 100 :attack-message "zapped" :area 'cave)
(defmonster slug :name "Slug" :hp 100 :attack 60 :attack-message "slimed" :area 'cave)
(defmonster willowisp :name "Will O' the Wisp" :hp 1 :attack 70 :attack-message "ignites" :area 'cave)
(defmonster cave-orc :name "Cave Orc" :hp 300 :attack 100 :attack-message "bashed" :area 'cave)
(defmonster hell-beast :name "Hell Beast" :hp 600 :attack 300 :attack-message "threw flame at" :area 'cave)

(defboss the-blind :name "The Blind" :hp 2000 :attack 400 :attack-message "mind fucked") ;Boss of the Cave

;;;;Ruins

(defmonster griffin :name "Griffin" :hp 200 :attack 30 :attack-message "clawed" :area 'ruins)
(defmonster ruins-toad :name "Ruins Toad" :hp 100 :attack 10 :attack-message "drugged" :area 'ruins)
(defmonster ruins-slug :name "Ruins Slug" :hp 100 :attack 20 :attack-message "slimed" :area 'ruins)
(defmonster ruins-wisp :name "Bruja" :hp 1 :attack 80 :attack-message "ignites" :area 'ruins)
(defmonster ruins-ogre :name "Ruins Ogre" :hp 400 :attack 20 :attack-message "bashed" :area 'ruins)
(defmonster hydra :name "Hydra" :hp 3000 :attack 1000 :attack-message "used several heads to bite" :area 'ruins)

(defboss the-ancient :name "The Ancient" :hp 5000 :attack 800 :attack-message "threw a boulder at") ;Boss of the Ruins

;;;;Forest

(defmonster centaur :name "Centaur" :hp 2000 :attack 30 :attack-message "gored" :area 'forest)
(defmonster hill-orc :name "Hill Orc" :hp 1500 :attack 10 :attack-message "hacked" :area 'forest)
(defmonster giant-shroom :name "Giant Mushroom" :hp 4000 :attack 5 :attack-message "spored" :area 'forest)
(defmonster sentient-tree :name "Sentient Tree" :hp 6000 :attack 5 :attack-message "slapped" :area 'forest)
(defmonster forest-wisp :name "Forest Spirit" :hp 100 :attack 0 :attack-message "broke leaves on" :area 'forest)
(defmonster forest-beast :name "Forest Beast" :hp 5000 :attack 600 :attack-message "strangled" :area 'forest)

(defboss wood-dragon :name "Wood Dragon - Guardian of the Forest" :hp 100000 :attack 400 :attack-message "used vines to whip") ;Boss of the Forest

;;finale
(defboss mountain-dragon :name "Mountain Dragon - King of Dragons" :hp 20000 :attack 3000)


#|(defun monster-attack-message (monster dmg)
  (cond ((swamp-thing-t monster)
	 (format t "The ~A attacks you for ~D!" (monster-name monster) dmg))
	))

(defgeneric monster-init (monster)
  (:documentation "Creates a monster."))
(defgeneric monster-attack (monster)
  (:documentation "Calculates attack and shit")
  (setf (player-cur-HP player) (- (player-cur-HP player) (- (monster-attack monster) (+ (player-bon-def player) (player-def player)))))
  )
(defmethod monster-attack (monster)
  (format t "The ~A attacks you for ~D!" (monster-name monster) (- (monster-attack monster) (+ (player-bon-def player) (player-def player))))
  )

;;;;Possible monsters are added every 10 depths
;;;;Depths are added for each Main path taken.

;;;;Swamp

(defstruct (swamp-thing (:include monster)))

(defmethod monster-init ((monster swamp-thing))
  (setf (monster-name enemy) "Swamp Thing")
  (setf (monster-HP enemy) 20)
  (setf (monster-atk enemy) 2))

(defstruct (toad (:include monster)))

(defmethod monster-init ((monster toad))
  (setf (monster-name enemy) "Toad")
  (setf (monster-HP enemy) 50)
  (setf (monster-atk enemy) 5))

;;;;Cave

(defstruct (eyeball (:include monster)))

(defmethod monster-init ((monster eyeball))
  (setf (monster-name enemy) "Floating Eyeball")
  (setf (monster-HP enemy) 100)
  (setf (monster-atk enemy) 10))

(defmethod monster-attack ((monster eyeball))
  (format t "The Floating Eyeball zapped you for ~D damage!" (- (monster-attack monster) (+ (player-bon-def player) (player-def player))))
  )

(defstruct hell-beast (:include monster))

(defmethod monster-init ((monster hell-beast))
  (setf (monster-name enemy) "Hell Beast")
  (setf (monster-HP enemy) 300)
  (setf (monster-atk enemy) 50))

(defstruct slug (:include monster))

(defmethod monster-init ((monster slug))
  (setf (monster-name enemy) "Slug")
  (setf (monster-HP enemy) 50)
  (setf (monster-atk enemy) 5))

(defstruct cave-orc (:include monster))

;;;;Ruins

(defstruct griffin (:include monster))

(defmethod monster-init ((monster griffin))
  (setf (monster-name enemy) "Griffin")
  (setf (monster-HP enemy) 200)
  (setf (monster-atk enemy) 30))

;;;;Forest

(defstruct centaur (:include monster))

(defmethod monster-init ((monster centaur))
  (setf (monster-name enemy) "Centaur")
  (setf (monster-hp enemy) 100)
  (setf (monster-atk enemy) 30))

(defmethod monster-attack ((monster centaur))
  (format t "The Centaur gored you for ~D damage!" (- (monster-attack monster) (+ (player-bon-def player) (player-def player))))
  )

(defstruct hill-orc (:include monster))


;;;;Bosses

(defstruct wood-dragon (:include monster)) ;Boss of the Forest

(defstruct the-deep (:include monster)) ;Boss of the Swamp

(defstruct the-ancient (:include monster)) ;Boss of the Ruins

(defstruct the-blind (:include monster)) ;Boss of the Cave

(defstruct mountain-dragon (:include monster)) ;Final Boss

|#
