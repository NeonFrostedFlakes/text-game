(defvar optional-path nil)
(defun increase-depth ()
  (incf depth 1)
  (setf sub-state 'depth))

(defvar event-choice nil)
(defun reach-into-water ()
  (if optional-path
      (setf event-choice (random 50)
	    sub-state 'reach)
      (setf message "Invalid choice. Please select a valid option.")))

(defmacro random-event-tree (descriptions &body body)
  `(let ((event-num (random (length ,descriptions))))
     (format t (elt ,descriptions event-num))
     ,@body))

(defun explore-side-cave ()
  )

(defun shuffle-rocks ()
  )

(defun RUINS_ALT ()
  )

(defun FOREST_ALT ()
  )


